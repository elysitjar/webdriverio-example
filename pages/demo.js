class Demo{

    get pageHeader () { return $('h1.title'); }
    get subHeader () { return $('div.content')}   
    get demoForm () { return $('div.column>form.demo-form')}

}
module.exports = new Demo();
