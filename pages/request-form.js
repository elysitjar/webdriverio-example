class RequestForm {
    get firstName() { return $('#firstName') }
    get lastName() { return $('#lastName') }
    get email() { return $('#email') }
    get companyName() { return $('#companyName') }
    get companyType() { return $('#companyType') }
    get phone() { return $('#phone') }
    get submitBtn() { return $('button') }
    get errorMsg() { return $('#class.error-msg') }

    enterFirstName(text){
        this.firstName.waitForDisplayed();
        this.firstName.setValue(text);
    }
    enterLastName(text){
        this.lastName.waitForDisplayed();
        this.lastName.setValue(text);
    }
    enterEmail(text){
        this.email.waitForDisplayed();
        this.email.setValue(text);
    }
    enterCompanyName(text){
        this.companyName.waitForDisplayed();
        this.companyName.setValue(text);
    }
    enterCompanyType(text){
        this.companyType.waitForDisplayed();
        this.companyType.setValue(text);
    }
    enterPhone(text){
        this.phone.waitForDisplayed();
        this.phone.setValue(text);
    }

    clickOnSubmit(){
        this.submitBtn.waitForDisplayed();
        this.submitBtn.click();

    }
}
module.exports = new RequestForm();
