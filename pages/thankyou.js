class ThankYou{

    get pageHeader () { return $('h1.title'); }
    get subHeader () { return $('h2.subtitle'); }  

}
module.exports = new ThankYou();
